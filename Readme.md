# Elevators Test

For this test I created an application with Symfony 5 framework to show the report of the simulation requested by PDF.

I created a simple HTML because it's not the evaluated part of this test. If you want to see my fronted skills let me know, and I improve that part.

You can test this with docker or with local symfony development server if you have php and composer.

## Requirements
- Docker
  
  OR
- Composer (For local development server)
- php (For local development server)
- symfony (For local development server)

## Steps to test with DOCKER
- Execute `docker-compose up -d build`
- Access to http://127.0.0.1:8000
- To execute tests `docker-compose run php-cli php vendor/bin/simple-phpunit tests`


## Steps to test with Symfony Local Development Server
- Enter to symfony dir
- Execute `composer install`
- After this execute `symfony server:start`



