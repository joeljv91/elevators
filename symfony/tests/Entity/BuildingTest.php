<?php

namespace App\Tests\Entity;

use App\Entity\Building;
use App\Entity\Schedule;
use PHPUnit\Framework\TestCase;

class BuildingTest extends TestCase
{
    /**
     * Static method to create test building
     *
     * @return Building
     */
    static function createBuilding()
    {
        return new Building(3, 4, new Schedule(9, 20, []));
    }

    public function testCreationAttributes()
    {
        $building = static::createBuilding();

        // Test attributes
        $this->assertEquals(3, $building->getFloors());
        $this->assertEquals(4, count($building->getElevators()));
        $this->assertInstanceOf(Schedule::class, $building->getSchedule());
    }

    public function testValidFloor()
    {
        $building = static::createBuilding();

        $this->assertTrue($building->isValidFloor(3));
        $this->assertFalse($building->isValidFloor(5));
    }
}