<?php

namespace App\Tests\Entity;

use App\Entity\Elevator;
use App\Entity\UserRequest;
use PHPUnit\Framework\TestCase;

class ElevatorTest extends TestCase
{

    /**
     * Create elevator and building for this test.
     *
     * @return Elevator
     */
    static function createElevator()
    {
        $building = BuildingTest::createBuilding();

        return new Elevator($building);
    }

    /**
     * Test elevator creation and attributes
     */
    public function testCreation()
    {
        $elevator = static::createElevator();

        $this->assertFalse($elevator->isGoingDown());
        $this->assertFalse($elevator->isGoingUp());
        $this->assertTrue($elevator->isStoped());
        $this->assertEquals(0, $elevator->getCurrentFloor());
        $this->assertEmpty($elevator->getDestinationFloors());
        $this->assertEquals(0, $elevator->getTraveledFloors());
    }

    public function testDirections()
    {
        $elevator = static::createElevator();
        $elevator->destination(2);

        $this->assertTrue($elevator->isGoingUp());
        $this->assertFalse($elevator->isGoingDown());

        $elevator = static::createElevator();
        $elevator->setCurrentFloor(3);
        $elevator->destination(1);

        $this->assertFalse($elevator->isGoingUp());
        $this->assertTrue($elevator->isGoingDown());
    }

    public function testTravel()
    {
        $elevator = static::createElevator();

        // Is floor 0
        $this->assertEquals(0, $elevator->getCurrentFloor());

        // Add destination
        $elevator->destination(2);

        // Travel 3 times 2 for travel to floor 1 for open the doors and let in/out
        $elevator->travel();
        $elevator->travel();
        $elevator->travel();

        // Validate current floor, destinations empty and traveled floors
        $this->assertEquals(2, $elevator->getCurrentFloor());
        $this->assertEmpty($elevator->getDestinationFloors());
        $this->assertEquals(2, $elevator->getTraveledFloors());

        $elevator = static::createElevator();
        $elevator->setCurrentFloor(3);

        // Added destinations without order
        $elevator->destination(1);
        $elevator->destination(2);

        // Is floor 3
        $this->assertEquals(3, $elevator->getCurrentFloor());

        // Travel elevator for all floors
        $elevator->travel();
        $elevator->travel();
        $elevator->travel();
        $elevator->travel();

        // Validate data
        $this->assertEquals(1, $elevator->getCurrentFloor());
        $this->assertEmpty($elevator->getDestinationFloors());
        $this->assertEquals(2, $elevator->getTraveledFloors());
    }

    public function testStateChange() {

        // Elevator stopped
        $elevator = static::createElevator();
        $elevator->setCurrentFloor(0);
        $this->assertTrue($elevator->isStoped());
        $this->assertFalse($elevator->isGoingDown());
        $this->assertFalse($elevator->isGoingUp());

        // Elevator is going up
        $elevator = static::createElevator();
        $elevator->setCurrentFloor(0);
        $elevator->destination(2);
        $this->assertFalse($elevator->isStoped());
        $this->assertFalse($elevator->isGoingDown());
        $this->assertTrue($elevator->isGoingUp());

        // Elevator is going down
        $elevator = static::createElevator();
        $elevator->setCurrentFloor(3);
        $elevator->destination(0);
        $this->assertFalse($elevator->isStoped());
        $this->assertTrue($elevator->isGoingDown());
        $this->assertFalse($elevator->isGoingUp());
    }

    public function testCanAcceptUserRequest()
    {
        // Can accept user on the same floor and when is stopped
        $elevator = static::createElevator();
        $elevator->setCurrentFloor(0);
        $userRequest = new UserRequest(0, 3);
        $this->assertTrue($elevator->canAcceptUser($userRequest));

        // Can accept user on the middle floor and when is ongoing destination up
        $elevator = static::createElevator();
        $elevator->setCurrentFloor(0);
        $elevator->destination(3);
        $userRequest = new UserRequest(1, 2);
        $this->assertTrue($elevator->canAcceptUser($userRequest));

        // Can accept user on the middle floor and when is ongoing destination down
        $elevator = static::createElevator();
        $elevator->setCurrentFloor(2);
        $elevator->destination(0);
        $userRequest = new UserRequest(1, 0);
        $this->assertTrue($elevator->canAcceptUser($userRequest));


        // User is going up and elevator is going down, can't accept user
        $elevator = static::createElevator();
        $elevator->setCurrentFloor(2);
        $elevator->destination(0);
        $userRequest = new UserRequest(0, 2);
        $this->assertFalse($elevator->canAcceptUser($userRequest));

        // Elevator is going up but finish before can get user
        $elevator = static::createElevator();
        $elevator->setCurrentFloor(0);
        $elevator->destination(2);
        $userRequest = new UserRequest(2, 3);
        $this->assertTrue($elevator->canAcceptUser($userRequest));
    }

    public function testTimeToFloor()
    {
        // Test user request from 0 to 3 and elevator is on floor 0 stopped
        $elevator = static::createElevator();
        $elevator->setCurrentFloor(0);
        $userRequest = new UserRequest(0, 3);
        $this->assertEquals(0, $elevator->getTimeForSatisfyUserRequest($userRequest));

        // Test user request from 3 to 0 and elevator is on floor 3 stopped
        $elevator = static::createElevator();
        $elevator->setCurrentFloor(3);
        $userRequest = new UserRequest(3, 0);
        $this->assertEquals(0, $elevator->getTimeForSatisfyUserRequest($userRequest));

        // Test user request from 2 to 0 and elevator is on floor 3 destination 1
        $elevator = static::createElevator();
        $elevator->setCurrentFloor(3);
        $elevator->destination(1);
        $userRequest = new UserRequest(2, 0);
        $this->assertEquals(1, $elevator->getTimeForSatisfyUserRequest($userRequest));

        // Test user request from 2 to 3 and elevator is on floor 0 destination 2
        $elevator = static::createElevator();
        $elevator->setCurrentFloor(0);
        $elevator->destination(2);
        $userRequest = new UserRequest(2, 3);
        $this->assertEquals(2, $elevator->getTimeForSatisfyUserRequest($userRequest));

        // Test user request from 0 to 3 and elevator is on floor 3 destination 0
        $elevator = static::createElevator();
        $elevator->setCurrentFloor(3);
        $elevator->destination(0);
        $userRequest = new UserRequest(0, 3);
        $this->assertEquals(6, $elevator->getTimeForSatisfyUserRequest($userRequest));
    }
}