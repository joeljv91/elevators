<?php

namespace App\Tests\Entity;

use App\Entity\Schedule;
use App\Entity\UserRequest;
use PHPUnit\Framework\TestCase;

class ScheduleTest extends TestCase
{

    /**
     * Static method to create schedule for this test case
     *
     * @return Schedule
     */
    static function createSchedule() {
        return new Schedule(9,11,[SequenceTest::createSequence()]);
    }

    public function testCreation() {
        $schedule = static::createSchedule();

        $this->assertEquals(9, $schedule->getStartHour());
        $this->assertEquals(11,$schedule->getEndHour());
        $this->assertNotEmpty($schedule->getSequences());
    }

}