<?php

namespace App\Tests\Entity;

use App\Entity\Sequence;
use PHPUnit\Framework\TestCase;

class SequenceTest extends TestCase
{
    static function createSequence() {
        return new Sequence(9,00,11,30,5,[]);
    }

    /**
     * Test time method success calculation
     */
    public function testIsTime() {
        $sequence = static::createSequence();

        $this->assertTrue($sequence->isSecuenceTime(10,30));
        $this->assertFalse($sequence->isSecuenceTime(11,31));
    }

}