<?php

namespace App\Tests\Entity;

use App\Entity\UserRequest;
use PHPUnit\Framework\TestCase;

class UserRequestTest extends TestCase
{
    /**
     * Create user request for testing
     *
     * @return UserRequest
     */
    static function createUserRequest()
    {
        return new UserRequest(1, 3);
    }

    public function testCreationAttributes()
    {
        $userRequest = static::createUserRequest();

        $this->assertEquals(1, $userRequest->getOrigin());
        $this->assertEquals(3, $userRequest->getDestination());
    }
}