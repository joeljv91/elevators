<?php

namespace App\Tests\Service;

use App\Entity\Schedule;
use App\Service\ExampleSchedule;
use PHPUnit\Framework\TestCase;

/**
 * Class ExampleScheduleTest
 *
 * @package App\Tests\Service
 */
class ExampleScheduleTest extends TestCase
{

    static function createExampleSchedule()
    {
        return new ExampleSchedule();
    }

    public function testDataLength()
    {
        $exampleSchedule = static::createExampleSchedule();
        $data = $exampleSchedule->getExampleSchedule();

        // Validate instance of
        $this->assertInstanceOf(Schedule::class, $data);
        $this->assertCount(4, $data->getSequences());
    }
}