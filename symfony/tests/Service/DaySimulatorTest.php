<?php

namespace App\Tests\Service;

use App\Entity\Building;
use App\Entity\Schedule;
use App\Entity\Sequence;
use App\Entity\UserRequest;
use App\Service\DaySimulator;
use PHPUnit\Framework\TestCase;

/**
 * Class DaySimulatorTest
 *
 * @package App\Tests\Service
 */
class DaySimulatorTest extends TestCase
{

    static function createBuildingSimulation()
    {
        return new Building(2, 2, new Schedule(
            9, 11, [
                new Sequence(9, 0, 11, 00, 5, [
                    new UserRequest(0, 2),
                    new UserRequest(2, 0)
                ])
            ]
        ));
    }

    static function createDaySimulator()
    {
        return new DaySimulator();
    }

    public function testReportData()
    {
        // Create building for this test
        $simulator = static::createDaySimulator();
        $report = $simulator->getSimulatedReport(static::createBuildingSimulation());

        // Validate data hours/minutes
        $this->assertCount(2, $report);
        $this->assertCount(60, $report[9]);
    }

    public function testMoveElevators()
    {
        // Create building for this test
        $simulator = static::createDaySimulator();
        $simulator->setBuilding(static::createBuildingSimulation());

        // Validate all is stopped
        foreach ($simulator->getBuilding()->getElevators() as $elevator) {
            $this->assertEquals(0, $elevator->getCurrentFloor());
            $this->assertEmpty($elevator->getDestinationFloors());
        }

        // Simulate report
        $simulator->getSimulatedReport(static::createBuildingSimulation());

        // Validate all user request are satisfied
        $this->assertEmpty($simulator->getUserRequests());
    }
}