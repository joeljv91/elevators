<?php

namespace App\Controller;

use App\Service\ExampleSchedule;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Service\DaySimulator;
use App\Entity\Building;

class ReportController extends AbstractController
{
    /**
     * @Route("/")
     *
     * @param DaySimulator $daySimulator
     * @param ExampleSchedule $exampleSchedule
     *
     * @return Response
     */
    public function index(DaySimulator $daySimulator, ExampleSchedule $exampleSchedule): Response
    {
        // Create or get building
        $building = new Building(3, 3, $exampleSchedule->getExampleSchedule());

        return $this->render('report.html.twig', [
            "simulationData" => $daySimulator->getSimulatedReport($building)
        ]);
    }
}