<?php

namespace App\Entity;

use http\Client\Curl\User;
use phpDocumentor\Reflection\Types\This;

/**
 * Class Elevator
 *
 * On that model structure elevator has building dependency to have floor validation but it's not necessary. On that
 * case I considered correct to made this way to validate input floors for each building to reuse it this app.
 *
 * @package App\Entity
 */
class Elevator
{
    const STATE_STOP = 0;
    const STATE_UP = 1;
    const STATE_DOWN = 2;

    /**
     * Building for this elevator
     *
     * @var Building
     */
    private $_building;

    /**
     * @var int Current floor location
     */
    private $_currentFloor;

    /**
     * Destination floors for this elevator
     *
     * @var array destinationFloors
     */
    private $_destinationFloors = [];

    /**
     * Traveled floors by this elevator
     *
     * @var int
     */
    private $_traveledFloors = 0;

    /**
     * Elevator status
     *
     * @var int
     */
    private $_state = self::STATE_STOP;

    /**
     * Elevator constructor.
     *
     * @param Building $_building
     */
    public function __construct(Building $_building)
    {
        $this->_building = $_building;
        $this->_currentFloor = $this->_building->getFirstFloor();
    }

    /**
     * @return array
     */
    public function getDestinationFloors()
    {
        return $this->_destinationFloors;
    }

    /**
     * Return if elevator is stopped
     *
     * @return bool
     */
    public function isStoped()
    {
        return $this->_state == self::STATE_STOP;
    }

    /**
     * Return if elevator is going up
     *
     * @return bool
     */
    public function isGoingUp()
    {
        return $this->_state == self::STATE_UP;
    }

    /**
     * Return if elevator is going down
     *
     * @return bool
     */
    public function isGoingDown()
    {
        return $this->_state == self::STATE_DOWN;
    }

    /**
     * Elevator request
     *
     * @param $floor
     *
     * @return bool
     */
    public function destination($floor)
    {
        if (!$this->_building->isValidFloor($floor) or $this->_currentFloor == $floor) {
            // Invalid floor
            return false;
        }

        // If is empty calculate direction down or up
        if (empty($this->_destinationFloors)) {
            if ($this->_currentFloor == $floor) {
                $this->_state = self::STATE_STOP;
            } elseif ($this->_currentFloor > $floor) {
                $this->_state = self::STATE_DOWN;
            } else if ($this->_currentFloor < $floor) {
                $this->_state = self::STATE_UP;
            }
        }

        // Add destination and order destinations
        if (!in_array($floor, $this->_destinationFloors)) {

            $this->_destinationFloors[] = $floor;
            if ($this->isGoingUp()) {
                sort($this->_destinationFloors);
            } else if ($this->isGoingDown()) {
                rsort($this->_destinationFloors);
            }

        }
    }

    /**
     * @param int $currentFloor
     * @return Elevator
     */
    public function setCurrentFloor(int $currentFloor): Elevator
    {
        $this->_currentFloor = $currentFloor;
        return $this;
    }

    /**
     * Get current floor
     *
     * @return int
     */
    public function getCurrentFloor()
    {
        return $this->_currentFloor;
    }

    /**
     * @return int
     */
    public function getTraveledFloors(): int
    {
        return $this->_traveledFloors;
    }

    /**
     * Move elevator
     */
    public function travel()
    {

        // If destination is empty
        if (!empty($this->_destinationFloors)) {
            $nextRequestedFloor = $this->_destinationFloors[0];

            // Check if we need to open doors and get people
            if ($this->_currentFloor == $nextRequestedFloor) {

                // Remove the element and wait for next travel move
                array_shift($this->_destinationFloors);
            } else if ($nextRequestedFloor > $this->_currentFloor) {
                $this->_currentFloor++;
                $this->_traveledFloors++;
            } else if ($nextRequestedFloor < $this->_currentFloor) {
                $this->_currentFloor--;
                $this->_traveledFloors++;
            }

            // If no destinations elevator is stopped
            if (empty($this->_destinationFloors)) {
                $this->_state = self::STATE_STOP;
            }
        }
    }

    /**
     * Return if elevator is available to get this user request
     *
     * @param UserRequest $userRequest
     *
     * @return bool
     */
    public function canAcceptUser(UserRequest $userRequest)
    {
        $acceptRequest = false;

        if ($userRequest->isGoingUp() and $this->isGoingUp()
            and $this->_currentFloor <= $userRequest->getOrigin()) {
            $acceptRequest = true;
        }

        if ($userRequest->isGoingDown() and $this->isGoingDown()
            and $this->_currentFloor >= $userRequest->getOrigin()) {
            $acceptRequest = true;
        }

        if ($this->isStoped() and empty($this->getDestinationFloors())) {
            $acceptRequest = true;
        }

        return $acceptRequest;
    }

    public function getTimeForSatisfyUserRequest(UserRequest $userRequest)
    {
        $userOrigin = $userRequest->getOrigin();
        $previousTraveledFloors = 0;
        if(!$this->canAcceptUser($userRequest)) {
            // Elevator can't accept this user because is in the other way calculate initial traveled floors
            if($this->isGoingUp()) {
                $maxDest = max($this->getDestinationFloors());
                $previousTraveledFloors = $maxDest - $this->_currentFloor;
            } elseif($this->isGoingDown()) {
                $minDest = min($this->getDestinationFloors());
                $previousTraveledFloors = $this->_currentFloor - $minDest;
            }
        }

        if($this->_currentFloor == $userOrigin) {
            $floorsToUserRequest = 0;
        } elseif($this->_currentFloor <= $userOrigin) {
            $floorsToUserRequest = $this->_currentFloor + $userOrigin ;
        } else {
            $floorsToUserRequest = $this->_currentFloor - $userOrigin ;
        }
        return $previousTraveledFloors + $floorsToUserRequest;
    }
}