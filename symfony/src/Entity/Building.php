<?php

namespace App\Entity;

/**
 * Class Building
 *
 * @package App\Entity
 */
class Building
{
    /**
     * Number of floors for this building
     *
     * @var int
     */
    private $_floors;

    /**
     * Elevators for this building
     *
     * @var Elevator[]
     */
    private $_elevators;

    /**
     * Schedule for this building
     *
     * @var Schedule
     */
    private $_schedule;

    /**
     * Building constructor.
     *
     * @param int $floors
     * @param int $totalElevators - Number of elevators for this building
     * @param Schedule $schedule
     */
    public function __construct(int $floors, int $totalElevators, Schedule $schedule)
    {
        $this->_floors = $floors;
        $this->_schedule = $schedule;

        // Create elevators with received input
        $i = 0;
        while ($i < $totalElevators) {
            $this->_elevators[] = new Elevator($this);
            $i++;
        }
    }

    /**
     * @return int
     */
    public function getFloors(): int
    {
        return $this->_floors;
    }

    /**
     * Return if floor is valid for this building
     *
     * @param $floor
     *
     * @return bool
     */
    public function isValidFloor($floor)
    {
        if ($floor <= $this->_floors) {
            return true;
        }

        return false;
    }

    /**
     * Return first floor
     *
     * We suppose all buildings are going to have a ground floor (0) but with this method we can change or calculate
     * the first floor. For example if building has parking.
     *
     * @return int
     */
    public function getFirstFloor()
    {
        return 0;
    }

    /**
     * @return Elevator[]
     */
    public function getElevators(): array
    {
        return $this->_elevators;
    }

    /**
     * @return Schedule
     */
    public function getSchedule(): Schedule
    {
        return $this->_schedule;
    }
}