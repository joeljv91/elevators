<?php

namespace App\Entity;

/**
 * Class Schedule
 *
 * Class to organize day simulation with all the required data and secuences
 *
 * @package App\Entity
 */
class Schedule
{

    /**
     * @var int $_startHour
     */
    private $_startHour;

    /**
     * @var int $_endHour
     */
    private $_endHour;

    /**
     * @var Sequence[]
     */
    private $_sequences;

    /**
     * Schedule constructor.
     *
     * @param int $_startHour
     * @param int $_endHour
     * @param Sequence[] $_sequences
     */
    public function __construct(int $_startHour, int $_endHour, array $_sequences)
    {
        $this->_startHour = $_startHour;
        $this->_endHour = $_endHour;
        $this->_sequences = $_sequences;
    }

    /**
     * @return int
     */
    public function getStartHour(): int
    {
        return $this->_startHour;
    }

    /**
     * @return int
     */
    public function getEndHour(): int
    {
        return $this->_endHour;
    }

    /**
     * @return Sequence[]
     */
    public function getSequences(): array
    {
        return $this->_sequences;
    }
}