<?php

namespace App\Entity;

/**
 * Class Secuence
 *
 * @package App\Entity
 */
class Sequence
{
    /**
     * @var int
     */
    private $_startHour;

    /**
     * @var int
     */
    private $_startMinute;

    /**
     * @var int
     */
    private $_endHour;

    /**
     * @var int
     */
    private $_endMinute;

    /**
     * Every X minutes
     *
     * @var int
     */
    private $_every;

    /**
     * Array of user requests
     *
     * @var UserRequest[]
     */
    private $_userRequests;

    /**
     * Secuence constructor.
     * @param int $_startHour
     * @param int $_startMinute
     * @param int $_endHour
     * @param int $_endMinute
     * @param int $_every
     * @param UserRequest[] $_userRequests
     */
    public function __construct($_startHour, $_startMinute, $_endHour, $_endMinute, $_every, array $_userRequests)
    {
        $this->_startHour = $_startHour;
        $this->_startMinute = $_startMinute;
        $this->_endHour = $_endHour;
        $this->_endMinute = $_endMinute;
        $this->_every = $_every;
        $this->_userRequests = $_userRequests;
    }

    /**
     * Check if is time for this secuence
     *
     * @param $hour
     * @param $minutes
     *
     * @return bool
     */
    public function isSecuenceTime($hour, $minutes)
    {
        return ($this->_startHour < $hour or ($this->_startHour == $hour and $this->_startMinute <= $minutes))
            and ($this->_endHour > $hour or ($this->_endHour == $hour and $this->_endMinute >= $minutes))
            and $minutes % $this->_every == 0;
    }

    /**
     * @return UserRequest[]
     */
    public function getUserRequests()
    {
        return $this->_userRequests;
    }
}