<?php

namespace App\Entity;

/**
 * Class UserRequest
 *
 * @package App\Entity
 */
class UserRequest
{
    /**
     * @var int
     */
    private $_origin;

    /**
     * @var int
     */
    private $_destination;

    /**
     * UserRequest constructor.
     * @param int $_origin
     * @param int $_destination
     */
    public function __construct($_origin, $_destination)
    {
        $this->_origin = $_origin;
        $this->_destination = $_destination;
    }

    /**
     * @return int
     */
    public function getOrigin()
    {
        return $this->_origin;
    }

    /**
     * @return int
     */
    public function getDestination()
    {
        return $this->_destination;
    }

    /**
     * Return if user is going down
     *
     * @return bool
     */
    public function isGoingDown() {
        return $this->_origin > $this->_destination;
    }

    /**
     * Return if user is going up
     *
     * @return bool
     */
    public function isGoingUp() {
        return $this->_origin < $this->_destination;
    }
}