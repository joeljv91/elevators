<?php

namespace App\Service;

use App\Entity\Schedule;
use App\Entity\Sequence;
use App\Entity\UserRequest;

/**
 * Class ExampleSchedule
 *
 * Return a building schedule with start/end hours and sequences with user request for each sequence.
 *
 * @package App\Service
 */
class ExampleSchedule
{

    /**
     * Get example schedule
     *
     * @return Schedule
     */
    public function getExampleSchedule()
    {

        return new Schedule(9, 20, [
            new Sequence(9, 0, 11, 0, 5, [
                new UserRequest(0, 2)
            ]),
            new Sequence(9, 0, 10, 0, 10, [
                    new UserRequest(0, 1),
                ]
            ),
            new Sequence(11, 0, 18, 20, 20, [
                    new UserRequest(0, 1),
                    new UserRequest(0, 2),
                    new UserRequest(0, 3),
                ]
            ),
            new Sequence(14, 0, 15, 00, 4, [
                    new UserRequest(1, 0),
                    new UserRequest(2, 0),
                    new UserRequest(3, 0),
                ]
            )
        ]);
    }
}