<?php

namespace App\Service;

use App\Entity\Building;
use App\Entity\Elevator;
use App\Entity\UserRequest;

/**
 * Class DaySimulator
 *
 * @package App\Service
 */
class DaySimulator
{

    /**
     * Building for this schedule
     *
     * @var Building
     */
    private $_building;

    /**
     * Requests from users
     *
     * @var UserRequest[]
     */
    private $_userRequests;

    /**
     * Report data with this kind of structure
     *
     * [9 (hour) =>
     *      [1 (minute) =>
     *          ["id" => 0, "traveledFloors" => 210, "currentFloor" => 3],
     *          ...
     *      ...
     * ...
     *
     * @var array
     */
    private $_reportData = [];

    /**
     * @return Building
     */
    public function getBuilding(): Building
    {
        return $this->_building;
    }

    /**
     * @param Building $building
     * @return DaySimulator
     */
    public function setBuilding(Building $building): DaySimulator
    {
        $this->_building = $building;
        return $this;
    }

    /**
     *  Execute day simulator with hours and minutes
     *
     * @param Building $building
     *
     * @return array
     */
    public function getSimulatedReport(Building $building)
    {

        // Set building for class global usage
        $this->_building = $building;

        // Get building schedule for simulation
        $schedule = $this->_building->getSchedule();

        // Start generation
        $hour = $schedule->getStartHour();
        while ($hour < $schedule->getEndHour()) {

            // Add this hour to report data
            $this->_reportData[$hour] = [];

            $min = 0;
            while ($min < 60) {

                // Make schedule

                // Add next schedule
                foreach ($schedule->getSequences() as $sequence) {

                    // If is sequence time and every X minutes
                    if ($sequence->isSecuenceTime($hour, $min)) {

                        $userRequests = $sequence->getUserRequests();
                        foreach ($userRequests as $userRequest) {

                            // Append requests
                            $this->addUserRequest($userRequest);

                            // Call best elevator for this user request
                            $this->callElevator($userRequest);
                        }
                    }
                }

                // Execute move elevators since this user request are satisfied. We assume for this test, on each min
                // all user requests from origin to destination are satisfied. But on more complex development we can update
                // Elevator model to take care of minutes and add some waiting times between floor travelling and doors open/close
                while(!empty($this->getUserRequests()) or !$this->allUsersSatisfied()) {
                    $this->moveElevators();
                }

                // Get elevator reports for this hour/min
                $this->_reportData[$hour][$min] = $this->_getElevatorsReport();

                // Increment minutes
                $min++;
            }

            // Increment hour
            $hour++;
        }

        // Return report data
        return $this->_reportData;

    }

    public function allUsersSatisfied() {
        foreach($this->getBuilding()->getElevators() as $elevator) {
            if (!empty($elevator->getDestinationFloors())) {
                return false;
            }
        }

        return true;
    }

    /**
     * Call Elevator from origin and request floor
     *
     * @param UserRequest $userRequest
     *
     * @return bool
     */
    public function callElevator(UserRequest $userRequest)
    {

        // Calculate which elevator is available and order by time to origin
        $elevators = [];
        foreach ($this->_building->getElevators() as $number => $elevator) {

            // Calculate time for this elevator to get this user
            $time = $elevator->getTimeForSatisfyUserRequest($userRequest);

            // Add elevator with time
            $elevators[$number] = [
                "timeToOrigin" => $time,
                "number" => $number,
                "elevator" => $elevator
            ];
        }

        // Sort by time to origin and call fast elevator
        usort($elevators, function ($elevatora, $elevatorb) {
            return $elevatora["timeToOrigin"] > $elevatorb["timeToOrigin"];
        });

        if (!empty($elevators)) {
            /**
             * @var Elevator $elevator
             */
            $elevator = $elevators[0]["elevator"];

            // Add user request origin to call elevator
            $elevator->destination($userRequest->getOrigin());

            return true;
        }

        // No elevators available
        return false;
    }

    /**
     * Execute elevator movements
     */
    public function moveElevators()
    {

        foreach ($this->_building->getElevators() as $elevator) {

            // Check if any user request can be satisfied by this elevator
            foreach ($this->_userRequests as $userIndex => $userRequest) {

                // If user is in the same origin add user destinations
                if ($elevator->getCurrentFloor() == $userRequest->getOrigin()) {
                    $elevator->destination($userRequest->getDestination());

                    // Remove this request
                    unset($this->_userRequests[$userIndex]);
                }
            }

            // Travel this elevator
            $elevator->travel();
        }

    }

    /**
     * Get array with elevator reports structure with all building elevators
     *
     * Ex:
     *
     * [
     *      "id" => 1,
     *      "currentFloor"=> 2,
     *      "traveledFloors"=> 40
     * ]
     *
     * @return array
     */
    private function _getElevatorsReport()
    {

        $reportElevators = [];
        foreach ($this->_building->getElevators() as $index => $elevator) {
            $reportElevators[] = [
                "id" => $index,
                "currentFloor" => $elevator->getCurrentFloor(),
                "traveledFloors" => $elevator->getTraveledFloors(),
            ];
        }

        return $reportElevators;
    }

    /**
     * @return UserRequest[]
     */
    public function getUserRequests()
    {
        return $this->_userRequests;
    }

    /**
     * @param UserRequest $userRequest
     * @return DaySimulator
     */
    public function addUserRequest(UserRequest $userRequest)
    {
        $this->_userRequests[] = $userRequest;

        return $this;
    }
}
